//import 
import walletjs from 'ethereumjs-wallet';
import { Map,List } from 'immutable';
import { generateMnemonic, EthHdWallet } from 'eth-hd-wallet'

//Action

const CREATE_ACCOUNT = "CREATE_ACCOUNT";

//Action Creators

function createAccount(){
    return{
        type: CREATE_ACCOUNT
    };
}

// Reducer


const initialState = Map({
    accountAddress: List([
        Map({
            index : 0,
            address : "0x6df05046b553f0a0c092b84b9db8288add3c6ccd544c0557085aa8d8bc728a32"
        })
    ])
});


function reducer(state = initialState,action){
    const addressList = state.get('accountAddress');
    //console.log(addressList);
    switch(action.type){
        case CREATE_ACCOUNT:
            return applyCreateAccount(state, action);
        default : 
            return state;
    }

}

// Reducer Functions
function applyCreateAccount(state, action) {

    const addressList = state.get('accountAddress');
    let privateKey = walletjs.generate().getPrivateKeyString();
    let wallet = EthHdWallet.fromMnemonic("prize much air first chase glue message tenant honey toilet arrow recycle");
    console.log( wallet instanceof EthHdWallet );  //return TRUE
    console.log( generateMnemonic() );
    console.log("hd");
    return state.set('accountAddress', addressList.push(Map({

        index: addressList.toArray().length,
        address: privateKey
    })));
}

// Exports

const actionCreators = {
    createAccount
};
export { actionCreators };
// Default

export default reducer;
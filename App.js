/* @flow */
import React, { PureComponent } from 'react';

import 'babel-preset-react-native-web3/globals';

import Web3 from 'web3';

import Main from './src/Main';
import ListViewer from "./components/ListViewer";
import Container from './src/Container';
import reducer from './reducer';
import { Provider } from "react-redux";
import truffleConfig from './truffle';
import { createStore } from 'redux';

let store = createStore(reducer);
const network = truffleConfig.networks.ropsten;

// const TESTRPC_ADDRESS = `${network.protocol}://${network.host}:${network.port}`;

const TESTRPC_ADDRESS = `${network.protocol}://${network.host}/${network.key}`;
var crypto = require('crypto')




// var abc = crypto.createHash('sha1').update('abc').digest('hex')
type Props = *;
export default class App extends PureComponent<Props> {
	constructor(props: Props) {
		super(props);
		// Initialize web3 and set the provider to the testRPC.
		// set the provider you want from Web3.providers
		const web3Provider = new Web3.providers.HttpProvider(TESTRPC_ADDRESS);
		this.web3 = new Web3(web3Provider);
	}
	web3: *;
	render() {
		
		return (
			<Provider store={store} >
				<ListViewer />
			</Provider>
		);
	}
}

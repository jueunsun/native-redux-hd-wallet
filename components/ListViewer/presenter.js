import React, { Component } from "react";
import { TouchableOpacity, StyleSheet, View, Text, Button } from "react-native";

class ListViewer extends Component {

    render() {
        const { accountAddress, createAccount } = this.props;
        console.log(accountAddress.toArray());

        return (
            <View style={styles.container}>
                <Text style={styles.item}>Wallet Test APP</Text>
                {
                    accountAddress.toArray().map((item, index) => (
                        <TouchableOpacity key = {item.get("index")}  >
                            <Text>{item.get("address")}</Text>
                        </TouchableOpacity>
                    ))
                }
                <Button style={styles.item} onPress={createAccount} title="Create Account" />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 25
    },
    item: {
        padding: 10,
        fontSize: 18,
        height: 44,
    },
})

export default ListViewer;

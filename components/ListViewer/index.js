import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { actionCreators as keyActions } from "../../reducer";
import ListViewer from "./presenter";

function mapStateToProps(state) {
    const addressList = state.get('accountAddress');
    return {
        accountAddress : addressList
    };
  }
  
function mapDispatchToProps(dispatch) {
    return {
        createAccount: bindActionCreators(keyActions.createAccount, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ListViewer);
